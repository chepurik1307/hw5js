function createNewUser() {
    // Запитати ім'я від користувача
    const firstName = prompt("Введіть ваше ім'я:");
  
    // Запитати прізвище від користувача
    const lastName = prompt("Введіть ваше прізвище:");
  
    // Створити об'єкт newUser з властивостями firstName та lastName
    const newUser = {
      firstName: firstName,
      lastName: lastName,
      getLogin: function () {
        // Повернути першу літеру імені з'єднану з прізвищем в нижньому регістрі
        return (firstName[0] + lastName).toLowerCase();
      }
    };
  
    // Повернути об'єкт newUser
    return newUser;
  }
  
  // Створити юзера за допомогою функції createNewUser()
  const user = createNewUser();
  
  // Викликати у юзера метод getLogin() та вивести результат у консоль
  console.log(user.getLogin());
  